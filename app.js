const {
  Wechaty
} = require('wechaty')

const qrTerm = require('qrcode-terminal')
const axios = require('axios')
const xpath = require('xpath')
const dom = require('xmldom').DOMParser

const bot = new Wechaty({
  name: 'myWechatyBot',
})

bot
  .on('logout', onLogout)
  .on('login', onLogin)
  .on('scan', onScan)
  .on('error', onError)
  .on('message', onMessage)

bot.start()
  .catch(async e => {
    console.error('Bot start() fail:', e)
    await bot.stop()
    process.exit(-1)
  })

function onScan(qrcode, status) {
  qrTerm.generate(qrcode, {
    small: true
  })

  const qrcodeImageUrl = [
    'https://wechaty.js.org/qrcode/',
    encodeURIComponent(qrcode),
  ].join('')

  console.log(`[${status}] ${qrcodeImageUrl}\nScan QR Code above to log in: `)
}

function onLogin(user) {
  console.log(`${user.name()} login`)
  bot.say('Wechaty login').catch(console.error)
}

function onLogout(user) {
  console.log(`${user.name()} logouted`)
}

function onError(e) {
  console.error('Bot error:', e)
}

// ----------------------------------------------

const HEADERS = {
  'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36',
}


const GUESS = '/guess '

async function onMessage(msg) {
  console.log(msg.toString())

  if (msg.type() !== bot.Message.Type.Text) {
    return
  }

  guess(msg)
}

// ------------------------------------------

async function guess(msg) {
  const text = msg.text()
  if (!text.startsWith(GUESS)) {
    return
  }
  const value = text.replace(GUESS, '')

  if (value == "香菜") {
    await msg.say('陈泽香菜297 \nhttps://space.bilibili.com/22150589?from=search&seid=5939758591748693564')
    return
  }

  // jiki
  await wait(1000)

  const parser = new dom({
    locator: {},
    errorHandler: {
      warning: function (w) {},
      error: function (e) {},
      fatalError: function (e) {
        console.error(e)
      }
    }
  });

  try {
    var res = await axios
      .get(`https://jikipedia.com/search?phrase=${encodeURI(value)}`, {
        headers: HEADERS
      });

    const resultId = xpath.select(`//div[@class='tile tile-0']`, parser.parseFromString(res.data))[0]

    const dataId = resultId.attributes[0].value
    const dataCategory = resultId.attributes[2].value
    console.log(`${dataId} ${dataCategory}`)

    await wait(500)

    res = await axios.get(`https://jikipedia.com/${dataCategory}/${dataId}?action=lite-card`, {
      headers: HEADERS
    })

    const content = xpath.select(`//div[@class='content']//div[@class='brax-render render']`, parser.parseFromString(res.data))[0]
    const childNodes = content.childNodes

    const result = []
    for (let index = 0; index < childNodes.length; index++) {
      const element = content.childNodes[index];
      if (element.nodeName == "span") {
        const lastNode = element.childNodes[0].childNodes[0]
        if (lastNode != null) {
          result.push(lastNode.nodeValue)
        } else {
          result.push('\n')
        }
      }
    }
    const resultStr = result.join('')
    if (resultStr.includes(value)) {
      await msg.say(resultStr)
      return
    }
  } catch (error) {
    console.error(error)
  }

  // guess
  try {
    res = await axios
      .post('https://lab.magiconch.com/api/nbnhhsh/guess', {
        text: value
      }, {
        headers: HEADERS
      });
    await msg.say(`${res.data[0].name}: [${res.data[0].trans.join(',')}]`)
    return
  } catch (error) {
    console.error(error)
  }

  // fail
  await msg.say(`我不知道: ${value}`)
}


function wait(ms) {
  return new Promise(resolve => setTimeout((e) => resolve(), ms));
};


const welcome = `
  | __        __        _           _
  | \\ \\      / /__  ___| |__   __ _| |_ _   _
  |  \\ \\ /\\ / / _ \\/ __| '_ \\ / _\` | __| | | |
  |   \\ V  V /  __/ (__| | | | (_| | |_| |_| |
  |    \\_/\\_/ \\___|\\___|_| |_|\\__,_|\\__|\\__, |
  |                                     |___/
  
  =============== Powered by Wechaty ===============
  -------- https://github.com/chatie/wechaty --------
            Version: ${bot.version(true)}

  Please wait... I'm trying to login in...
  
  `
console.log(welcome)