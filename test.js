const xpath = require('xpath')
const dom = require('xmldom').DOMParser
const axios = require('axios')


const HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36',
}

async function guess(value) {

    if (value == "香菜") {
        await msg.say('陈泽香菜297 \nhttps://space.bilibili.com/22150589?from=search&seid=5939758591748693564')
        return
    }

    // jiki
    await wait(1000)

    const parser = new dom({
        locator: {},
        errorHandler: {
            warning: function (w) {},
            error: function (e) {},
            fatalError: function (e) {
                console.error(e)
            }
        }
    });

    try {
        var res = await axios
            .get(`https://jikipedia.com/search?phrase=${encodeURI(value)}`, {
                headers: HEADERS
            });

        const resultId = xpath.select(`//div[@class='tile tile-0']`, parser.parseFromString(res.data))[0]

        const dataId = resultId.attributes[0].value
        const dataCategory = resultId.attributes[2].value
        console.log(`${dataId} ${dataCategory}`)

        await wait(1000)

        res = await axios.get(`https://jikipedia.com/${dataCategory}/${dataId}?action=lite-card`, {
            headers: HEADERS
        })

        const content = xpath.select(`//div[@class='content']//div[@class='brax-render render']`, parser.parseFromString(res.data))[0]
        const childNodes = content.childNodes

        const result = []
        for (let index = 0; index < childNodes.length; index++) {
            const element = content.childNodes[index];
            if (element.nodeName == "span") {
                const lastNode = element.childNodes[0].childNodes[0]
                if (lastNode != null) {
                    result.push(lastNode.nodeValue)
                } else {
                    result.push('\n')
                }
            }
        }
        const resultStr = result.join('')
        if (resultStr.includes(value)) {
            await msg.say(resultStr)
            return
        }
    } catch (error) {
        console.error(error)
    }

    // guess
    try {
        res = await axios
            .post('https://lab.magiconch.com/api/nbnhhsh/guess', {
                text: value
            }, {
                headers: HEADERS
            });
        const result = res.data[0]
        await msg.say(`${result.name}: [${result.trans.join(',')}]`)
        return
    } catch (error) {
        console.error(error)
    }

    console.log(`陈泽香菜297 \nhttps://space.bilibili.com/22150589?from=search&seid=5939758591748693564`)

    await msg.say(`我不知道: ${value}`)

}

function wait(ms) {
    return new Promise(resolve => setTimeout((e) => resolve(), ms));
};